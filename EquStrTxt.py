"""Convert txt file to txt file, where each string is equal (append " ")

    План:
        1. Открываем EqualThatPls.txt файл, лежащий в той же директории, что и .py
        2. Построчно считываем исходный файл, ищем наибольщий по длине, запоминаем число
        3. Построчно считываем исходный файл, добавляем нужное число пробелов, заносим в итоговый файл

"""


from os import path #: find the script dir's abspath

HERE = path.abspath(path.dirname(__file__))

class EquStrTxt(object):

    """Nya (=^-^=)
    """

    def __init__(self):
        self.maxline = 0

    def findmaxline(self):
        """Load data, find max line
        """
        try:
            with open(path.join(HERE, 'EqualThatPls.txt'), 'r', encoding='utf8') as file:
                for line in file:
                    if len(line) > self.maxline:
                        self.maxline = len(line)
        except FileNotFoundError:
            print('FileNotFoundError')

    def createdonefile(self):
        """Load data 2d time (!), append " ", save done data
        """
        try:
            with open(path.join(HERE, 'EqualThatPls.txt'), 'r', encoding='utf8') as inpf:
                with open(path.join(HERE, 'EqualThatDone.txt'), 'w', encoding='utf8') as outf:
                    for line in inpf:
                        tempstr = line[:-1] + (' ' * (self.maxline - len(line)) + '\n')
                        outf.write(tempstr)
        except FileNotFoundError:
            print('FileNotFoundError')

def main():
    """The programm's head
    """
    objexp = EquStrTxt()
    objexp.findmaxline()
    objexp.createdonefile()

if __name__ == "__main__":
    main()
